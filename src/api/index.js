const api = {
  home: 'https://crowdfunding.sanberdev.com',
  api: 'https://crowdfunding.sanberdev.com/api',
};

export default api;

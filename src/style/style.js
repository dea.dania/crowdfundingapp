import {StyleSheet, Dimensions} from 'react-native';
import {color} from 'react-native-reanimated';
import colors from './colors';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  form: {
    // backgroundColor: colors.blue,
    flex: 1,
    marginHorizontal: width * 0.07,
  },
  textInput: {
    // backgroundColor: 'yellow',
    marginVertical: width * 0.025,
    padding: width * 0.03,
    borderBottomWidth: 1,
    borderRadius: 10,
  },
  textInputEyeContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // backgroundColor: 'yellow',
    marginVertical: width * 0.025,
    borderBottomWidth: 1,
    borderRadius: 10,
  },
  textInputEye: {
    paddingHorizontal: width * 0.025,
    width: '90%',
  },
  iconEye: {
    padding: 10,
  },
  header: {
    justifyContent: 'center',
    marginTop: height * 0.05,
    marginBottom: height * 0.03,
    marginHorizontal: width * 0.077,
    // backgroundColor: colors.grey,
  },
  headerLogged: {
    // justifyContent: 'center'
    alignItems: 'center',
    paddingVertical: height * 0.02,
    marginBottom: height * 0.02,
    flexDirection: 'row',
    // backgroundColor: colors.grey,
    borderBottomWidth: 0.5,
    borderStyle: 'dashed',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: height * 0.05,
    color: colors.black,
  },

  headerTextLogged: {
    fontWeight: 'bold',
    fontSize: height * 0.04,
    color: colors.black,
    // backgroundColor: colors.grey,
  },
  subHeadText: {
    fontFamily: 'RobotoRegular',
    fontSize: 20,
    marginTop: 10,
  },
  subFootText: {
    flexDirection: 'row',
    fontFamily: 'ComfortaaBold',
    fontSize: 20,
    marginRight: 30,
    justifyContent: 'flex-end',
  },
  slider: {
    flex: 1,
  },
  btnContainer: {
    marginBottom: height * 0.03,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  btnLogin: {
    height: height * 0.05,
    width: width * 0.9,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.main,
  },
  btnTextLogin: {
    fontSize: width * 0.04,
    fontWeight: 'bold',
    color: colors.white,
  },
  btnRegister: {
    // marginTop: 100,
    flexDirection: 'row',
    height: height * 0.05,
    width: width * 0.85,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colors.black,
    backgroundColor: 'transparent',
  },
  btnTextRegister: {
    fontSize: width * 0.04,
    fontWeight: 'bold',
    color: colors.black,
  },
  twoBtn: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    paddingVertical: height * 0.04,
    paddingHorizontal: height * 0.03,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  optionsBtn: {
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colors.black,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    width: '40%',
    height: '60%',
  },
  listContainer: {
    marginTop: height * 0.03,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  listContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgList: {
    width: width * 1.7,
    height: width * 1.1,
  },
  textList: {
    marginTop: height * 0.05,
    fontSize: width * 0.035,
    fontFamily: 'RobotoRegular',
    color: colors.black,
  },
  activeDotStyle: {
    width: width * 0.06,
    backgroundColor: colors.black,
  },
  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
  },

  underlineStyleHighLighted: {
    borderColor: '#03DAC6',
  },
  otpStyle: {
    color: colors.black,
    width: width * 0.85,
    height: 200,
    paddingHorizontal: 10,
    backgroundColor: colors.black,
  },
  profile: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: width * 0.05,
    paddingHorizontal: width * 0.05,
    backgroundColor: 'white',
    // justifyContent: 'space-around',
    // alignItems: 'flex-start',
  },
  img: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderRadius: 25,
  },
  profileText: {
    marginLeft: width * 0.05,
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: width * 0.01,
  },
  item: {
    marginVertical: width * 0.01,
  },
  subItemSaldo: {
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: width * 0.05,
    paddingVertical: width * 0.05,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subItem: {
    marginBottom: width * 0.01,
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: width * 0.05,
    paddingVertical: width * 0.05,
    alignItems: 'center',
  },
  itemText: {
    marginLeft: width * 0.06,
    fontSize: 14,
  },
  // home
  searchContainer: {
    flexDirection: 'row',
    marginTop: height * 0.025,
    marginHorizontal: width * 0.05,
    marginVertical: width * 0.025,
    backgroundColor: colors.lightGrey,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 10,
  },
  iconSearch: {
    padding: 10,
  },
  payContainer: {
    backgroundColor: 'yellow',
    flexDirection: 'row',
    margin: '5%',
    padding: '2.5%',
    justifyContent: 'space-between',
    borderRadius: 10,
  },
  paySaldoItem: {
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: colors.main,
  },
  payItemBtnsSaldo: {
    padding: '2%',
    // backgroundColor: 'grey',
    borderWidth: 0.5,
    borderRadius: 10,
  },
  payItemBtns: {
    padding: '2%',
    // backgroundColor: 'grey',
    borderWidth: 0.5,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sliderContainer: {
    backgroundColor: colors.lightGrey,
    marginHorizontal: width * 0.05,
    borderRadius: 10,
    backgroundColor: colors.main,
  },
  imgSlider: {
    width: '100%',
    height: undefined,
    aspectRatio: 1.5,
    borderRadius: 10,
  },
  menuContainer: {
    // backgroundColor: colors.lightGrey,
    marginTop: width * 0.05,
    marginHorizontal: width * 0.05,
    marginBottom: width * 0.05,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  menuItemBtns: {
    backgroundColor: colors.white,
    padding: width * 0.02,
  },
  urgentDonateContainer: {
    flex: 1,
    marginBottom: width * 0.05,
  },
  urgentDonateItem: {
    flexWrap: 'wrap',
    width: width * 0.5,
    // alignItems: 'flex-start',
    marginLeft: width * 0.035,
    // elevation: 5,
    backgroundColor: colors.lemon,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  textUrgCon: {flexDirection: 'row'},
  urgentDonateText: {marginTop: -30, flexWrap: 'wrap', padding: 5},
  donateUrgImg: {
    marginTop: -34,
    width: width * 0.5,
    height: undefined,
    aspectRatio: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  // Donate
  donateItem: {
    // flexWrap: 'wrap',
    // width: width * 0.5,
    backgroundColor: colors.lemon,
    marginHorizontal: width * 0.05,
    flexDirection: 'row',
    marginBottom: width * 0.025,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
  },
  donateImg: {
    width: width * 0.4,
    height: undefined,
    aspectRatio: 1,
    borderBottomLeftRadius: 10,
  },
  donateItemDescCon: {
    width: '70%',
    flexWrap: 'wrap',
  },
  donateItemDesc: {
    marginTop: width * 0.025,
    width: '70%',
    margin: width * 0.025,
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  donateTitleCon: {
    flexDirection: 'row',
  },
  donateItemTitle: {
    flexWrap: 'wrap',
    fontWeight: 'bold',
    fontSize: height * 0.02,
  },
  donateItemNominal: {
    fontWeight: 'bold',
  },
  // detail donasi
  headerDetailDonation: {
    flexDirection: 'row',
  },
  donateImgdetails: {
    width: width,
    height: undefined,
    aspectRatio: 1,
    borderRadius: 10,
    opacity: 0.75,
    marginLeft: -width * 0.085,
  },
  iconbackDonate: {
    // marginRight: -width * 0.085,
    marginTop: width * 0.04,
    color: 'black',
  },
  donateDescDetails: {
    flex: 1,
    margin: width * 0.05,
  },
  dDetailsItemTitle: {
    fontWeight: 'bold',
    fontSize: height * 0.03,
  },
  dDetailsItemDesc: {
    fontWeight: 'bold',
    marginTop: width * 0.05,
  },
  dDetailsItemText: {
    flexWrap: 'wrap',
  },
  dDetailsItemSender: {
    margin: width * 0.05,
  },
  senderInfo: {
    margin: width * 0.05,
    borderWidth: 0.5,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  iconAccountDonate: {
    margin: width * 0.05,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  modalWrapper: {
    flex: 1,
    backgroundColor: colors.white,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    paddingTop: width * 0.05,
    paddingHorizontal: width * 0.05,
  },
  moneyContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  donateText: {fontWeight: 'bold', fontSize: 20, margin: 20},
  btnMoney: {
    padding: 10,
    borderWidth: 0.5,
    width: width * 0.27,
  },
  chartContainer: {
    flex: 1,
    margin: width * 0.025,
  },
  // history donasi
  historyctionItem: {
    marginHorizontal: width * 0.07,
    marginVertical: width * 0.025,
  },
  historyctionItemTitle: {
    fontWeight: 'bold',
    fontSize: width * 0.05,
  },
  // new donasi
  imgNewDonate: {
    width: width,
    height: undefined,
    aspectRatio: 2,
    marginTop: -16,
    backgroundColor: colors.lightGrey,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textLabel: {
    // backgroundColor: colors.lightGrey,
    paddingVertical: height * 0.01,
    marginVertical: height * 0.01,
    fontSize: height * 0.025,
  },
  textInputDonasi: {
    // backgroundColor: colors.lemon,
    paddingHorizontal: width * 0.03,
    borderBottomWidth: 0.5,
    borderRadius: 5,
  },
  textAreaInput: {
    paddingHorizontal: width * 0.03,
    // backgroundColor: colors.lightGrey,
    height: 150,
    textAlignVertical: 'top',
    borderWidth: 0.5,
    borderRadius: 5,
  },
  editphoto: {
    marginTop: -(height * 0.07),
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});

export default styles;

const colors = {
  white: '#ffffff',
  blue: '#3a86ff',
  darkblue: '#088dc4',
  black: '#000000',
  grey: '#A0A0A0',
  lightGrey: '#dfe6e9',
  transparent: 'rgba(0, 0, 0, 0.5)',
  main: '#00BCD4',
  lemon: '#fffa65',
};

export default colors;

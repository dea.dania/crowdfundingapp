import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {
  Intro,
  Splash,
  OTP,
  Login,
  Register,
  SetupPassword,
  Profile,
  Home,
  Donasi,
  HistoryDonasi,
  DonasiDetails,
  Payment,
  Help,
  Statistic,
  Inbox,
  NewDonasi,
  EditProfile,
} from './screens';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const HomeStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const HomeNavigation = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <HomeStack.Screen
        name="Statistic"
        component={Statistic}
        options={{headerShown: false}}
      />
      <HomeStack.Screen
        name="Donasi"
        component={Donasi}
        options={{headerShown: false}}
      />
      <HomeStack.Screen
        name="Payment"
        component={Payment}
        options={{headerShown: false}}
      />
      <HomeStack.Screen
        name="NewDonasi"
        component={NewDonasi}
        options={{headerShown: false}}
      />
      <HomeStack.Screen
        name="HistoryDonasi"
        component={HistoryDonasi}
        options={{headerShown: false}}
      />
      <HomeStack.Screen
        name="DonasiDetails"
        component={DonasiDetails}
        options={{headerShown: false}}
      />
    </HomeStack.Navigator>
  );
};

const ProfileNavigation = () => {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      />
      <ProfileStack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{headerShown: false}}
      />
      <ProfileStack.Screen
        name="Help"
        component={Help}
        options={{headerShown: false}}
      />
    </ProfileStack.Navigator>
  );
};
const TabNavigation = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused ? 'ios-home' : 'ios-home-outline';
          } else if (route.name === 'Inbox') {
            iconName = focused ? 'ios-chatbubbles' : 'ios-chatbubbles-outline';
          } else if (route.name === 'Profile') {
            iconName = focused ? 'ios-person' : 'ios-person-outline';
          }
          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Stack.Screen
        name="Home"
        component={HomeNavigation}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Inbox"
        component={Inbox}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Profile"
        component={ProfileNavigation}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
};

const MainNavigation = ({initial}) => {
  return (
    <Stack.Navigator initialRouteName={initial}>
      <Stack.Screen
        name="Dashboard"
        component={TabNavigation}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen name="OTP" component={OTP} options={{headerShown: false}} />
      <Stack.Screen
        name="SetupPassword"
        component={SetupPassword}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};
const AppNavigation = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [theinitial, setInitial] = useState('');
  useEffect(() => {
    async function getInitial() {
      try {
        const value = await AsyncStorage.getItem('first');
        // console.log("value :=>",value);
        setInitial(value === null || value !== 'false' ? 'Intro' : 'Dashboard');
        // console.log(Profile);
      } catch (err) {
        console.log('err => s ', err);
      }
    }
    // console.log("the initial:=>", theinitial);
    getInitial();
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
  }, []);

  if (isLoading) {
    return <Splash />;
  }

  return (
    <NavigationContainer>
      <MainNavigation initial={theinitial} />
    </NavigationContainer>
  );
};

export default AppNavigation;

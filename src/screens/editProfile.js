import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  Modal,
  ScrollView,
  ActivityIndicator,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {Button} from '../components/Button';

import {RNCamera} from 'react-native-camera';
import {TextInputMask} from 'react-native-masked-text';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from '../style/style';
import colors from '../style/colors';
import api from '../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
const EditProfile = ({navigation}) => {
  let input = useRef(null);
  let camera = useRef(null);
  const [token, setToken] = useState('');
  const [isVisible, setIsVisible] = useState(false);
  const [data, setData] = useState({});
  const [photo, setPhoto] = useState(null);
  const [imguri, setImguri] = useState(null);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [editable, setEditable] = useState(false);
  const [isloading, setIsloading] = useState(true);
  const [type, setType] = useState('back');

  const [activity, setActivity] = useState(false);
  // function refreshPage() {
  //   window.location.reload(false);
  // }
  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        // console.log('token => ', token);
        if (token !== null) {
          getProfile(token);
          setToken(token);
        }
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
    // getProfile();
  }, []);
  const toggleCamera = () => {
    setType(type == 'back' ? 'front' : 'back');
  };
  const getProfile = (token) => {
    Axios.get(`${api.api}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        console.log('res = >', res);
        setName(res.data.data.profile.name);
        setEmail(res.data.data.profile.email);
        setImguri(res.data.data.profile.photo);
        setIsloading(false);
      })
      .catch((err) => {
        console.log('error get profile=>', {err});
      });
  };
  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.current.takePictureAsync(options);
      setPhoto(data);
      setIsVisible(false);
      // console.log('editaccount=< data > ', data);
    }
  };

  const editData = () => {
    setEditable(!editable);
  };

  const onSavePress = async () => {
    setActivity(true);
    const formData = new FormData();
    formData.append('name', name);
    if (photo !== null) {
      formData.append('photo', {
        uri: photo.uri,
        name: 'photo.jpg',
        type: 'image/jpg',
      });
    }
    Axios.post(`${api.api}/profile/update-profile`, formData, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((res) => {
        console.log('res save profile= >', res);
        // console.log(res.request);
        setActivity(false);
        navigation.replace('Profile');
      })
      .catch((err) => {
        console.log('error=>', err);
        setActivity(false);
        alert('Update Profile is Failed');
      });
  };
  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flexDirection: 'row', backgroundColor: colors.black}}>
          <TouchableOpacity onPress={() => toggleCamera()}>
            <View style={styles.btnToggleCamera}>
              <Ionicons
                name="camera-reverse-outline"
                size={50}
                color={colors.white}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <RNCamera style={{flex: 1}} type={type} ref={camera}></RNCamera>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              backgroundColor: colors.black,
            }}>
            <TouchableOpacity onPress={() => takePicture()}>
              <View style={styles.btnToggleCamera}>
                <Ionicons
                  name="camera-outline"
                  size={50}
                  color={colors.white}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <View style={styles.headerLogged}>
          <Ionicons
            style={styles.iconSearch}
            name="chevron-back"
            size={30}
            onPress={() => navigation.goBack()}
          />
          <Text style={styles.headerTextLogged}>Edit Profile</Text>
        </View>
        {isloading ? (
          <View style={styles.container}>
            <ActivityIndicator size="small" color="#30475e" />
          </View>
        ) : (
          <>
            {photo == null ? (
              <View style={styles.imgNewDonate}>
                <Image
                  source={{
                    uri: api.home + imguri + '?' + new Date(),
                    cache: 'reload',
                    headers: {Pragma: 'no-cache'},
                  }}
                  style={styles.imgNewDonate}
                />
              </View>
            ) : (
              <Image
                source={{
                  uri: photo.uri,
                  cache: 'reload',
                  headers: {Pragma: 'no-cache'},
                }}
                style={styles.imgNewDonate}
              />
            )}
            <View style={styles.editphoto}>
              <Ionicons
                name="camera-outline"
                size={50}
                color={colors.lemon}
                onPress={() => setIsVisible(true)}
              />
            </View>
            <View style={styles.form}>
              <Text style={styles.textLabel}>Nama Lengkap </Text>
              <View style={styles.textInputEyeContainer}>
                <TextInput
                  value={name}
                  editable={editable}
                  onChangeText={(val) => setName(val)}
                  style={styles.textInputEye}
                />
                <Ionicons
                  style={styles.iconEye}
                  name="pencil-outline"
                  size={20}
                  onPress={() => editData()}
                />
              </View>
              <Text style={styles.textLabel}>Email </Text>
              <TextInput
                value={email}
                editable={false}
                style={styles.textInputDonasi}
              />
              <Button style={styles.btnRegister} onPress={() => onSavePress()}>
                {activity == true ? (
                  <ActivityIndicator size="small" color="#30475e" />
                ) : undefined}
                <Text style={styles.btnTextRegister}>SIMPAN</Text>
              </Button>
            </View>
          </>
        )}
      </View>

      {renderCamera()}
    </>
  );
};

export default EditProfile;

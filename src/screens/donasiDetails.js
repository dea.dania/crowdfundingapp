import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  ScrollView,
  Modal,
  ActivityIndicator,
} from 'react-native';
import {Button} from '../components/Button';
import {TextInputMask} from 'react-native-masked-text';
import colors from '../style/colors';

import MaterialComIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleIcon from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from '../style/style';
import api from '../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
// import NumberFormat from 'react-number-format';

const optionsNominal = [10000, 20000, 50000, 100000, 200000, 500000];
const DonasiDetails = ({navigation, route}) => {
  const [title, setTitle] = useState(route.params.title);
  const [nominal, setNominal] = useState(route.params.donation);
  const [desc, setDesc] = useState(route.params.description);
  const [user, setUser] = useState(route.params.user);
  const [donate, setDonate] = useState(null);
  const [text, setText] = useState(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim  ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut  aliquip ex ea commodo consequat. Duis aute irure dolor in  reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla  pariatur. Excepteur sint occaecat cupidatat non proident, sunt in  culpa qui officia deserunt mollit anim id est laborum.',
  );
  const [isvisible, setIsvisible] = useState(false);
  const [token, setToken] = useState(null);
  const [profile, setProfile] = useState(null);
  const [activity, setActivity] = useState(false);

  useEffect(() => {
    async function getToken() {
      try {
        const tok = await AsyncStorage.getItem('token');
        // console.log('token => ', token);
        if (tok !== null) {
          setToken(tok);
          getProfile(tok);
        }
        // console.log(Profile);
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
  });
  const getProfile = (token) => {
    Axios.get(`${api.api}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        setProfile(res.data.data.profile);
        // console.log(Profile);
      })
      .catch((err) => {
        console.log('error=>', err);
      });
  };
  const donateMoneyPress = () => {
    setActivity(true);
    if (donate < 10000)
      return Alert.alert('', 'Nominal should be 10K or more', {
        text: 'OK',
        onPress: () => console.log('OK Pressed'),
      });
    const time = new Date().getTime();
    const donate_id = route.params.id;
    const body = {
      transaction_details: {
        order_id: `Donasi-${time}`,
        gross_amount: donate,
        donation_id: donate_id,
      },
      customer_details: {
        first_name: profile.name,
        email: profile.email,
      },
    };
    Axios.post(`${api.api}/donasi/generate-midtrans`, body, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'aplication/json',
        'Content-Type': 'aplication/json',
      },
    })
      .then((res) => {
        console.log('onpressres=>', res);
        if (res) {
          setIsvisible(!isvisible);
          setActivity(false);
          const data = res.data.data;
          navigation.navigate('Payment', data);
        }
      })
      .catch((err) => {
        console.log('onpresserr=>', {err});
      });
  };
  const renderNominal = ({item}) => {
    return (
      <Button onPress={() => setDonate(item)} style={styles.btnMoney}>
        <Text
          style={[
            styles.btnTextRegister,
            {backgroundColor: item == donate ? colors.lemon : undefined},
          ]}>
          {'Rp.' + item.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
        </Text>
      </Button>
    );
  };
  const capitalize = (s) => {
    if (typeof s !== 'string') return '';
    return s.charAt(0).toUpperCase() + s.slice(1);
  };
  const donasiNominal = () => {
    return (
      <Modal transparent={true} visible={isvisible}>
        <View style={styles.modalContainer}></View>
        <View style={styles.modalWrapper}>
          <View style={styles.textInputEyeContainer}>
            <Text>ISI NOMINAL</Text>
            <Ionicons
              style={styles.iconEye}
              name="close-outline"
              size={20}
              onPress={() => setIsvisible(false)}
            />
          </View>
          <TextInputMask
            type={'money'}
            style={styles.donateText}
            value={donate}
            includeRawValueInChangeText={true}
            onChangeText={(maskedText, rawText) => setDonate(rawText)}
            options={{
              precision: 0,
              separator: '',
              delimiter: ',',
              unit: 'Rp. ',
              suffixUnit: '',
            }}
          />
          <View style={styles.moneyContainer}>
            <FlatList
              contentContainerStyle={styles.moneyContainer}
              data={optionsNominal}
              renderItem={(item) => renderNominal(item)}
            />
          </View>
          <Button style={styles.btnRegister} onPress={() => donateMoneyPress()}>
            {activity == true ? (
              <ActivityIndicator size="small" color="#30475e" />
            ) : (
              <Text></Text>
            )}
            <Text style={styles.btnTextRegister}>LANJUT</Text>
          </Button>
        </View>
      </Modal>
    );
  };
  return (
    <>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.headerDetailDonation}>
            <Ionicons
              style={styles.iconbackDonate}
              name="arrow-back-circle-sharp"
              size={30}
              onPress={() => navigation.navigate('Donasi')}
            />
            <Image
              source={require('../assets/img/poverty-1028841_640.jpg')}
              style={styles.donateImgdetails}
            />
          </View>
          <View style={styles.donateDescDetails}>
            <Text numberOfLines={2} style={styles.dDetailsItemTitle}>
              {capitalize(title)}
            </Text>
            <Text>
              Dana yang dibutuhkan
              {nominal !== null
                ? ' Rp.' +
                  nominal.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
                : undefined}
            </Text>
            <Text style={styles.dDetailsItemDesc}>Deskripsi</Text>

            {desc !== null ? (
              <Text style={styles.dDetailsItemText}>{desc}</Text>
            ) : (
              <Text>belum ada deskripsi</Text>
            )}

            {/* <Text style={styles.dDetailsItemText}>{text.substring(90)}</Text> */}
            <Button
              style={styles.btnRegister}
              onPress={() => setIsvisible(true)}>
              <Text style={styles.btnTextRegister}>DONASI SEKARANG</Text>
            </Button>
          </View>
          <View style={styles.dDetailsItemSender}>
            <Text style={styles.dDetailsItemTitle}>
              Informasi Penggalang Dana
            </Text>
            <View style={styles.senderInfo}>
              <MaterialComIcon
                style={styles.iconAccountDonate}
                name="account"
                size={40}
              />
              <Text style={styles.dDetailsItemSender}>{user.name}</Text>
            </View>
          </View>
        </View>
        {donasiNominal()}
      </ScrollView>
    </>
  );
};

export default DonasiDetails;

import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import MaterialComIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleIcon from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from '../style/style';
import api from '../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {FlatList} from 'react-native-gesture-handler';
const donasi = ({navigation}) => {
  const [donasi, setDonasi] = useState(null);
  const [len, setLen] = useState(null);
  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        // console.log('token => ', token);
        getDonasilist(token);
        // console.log(Profile);
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
  });
  const getDonasilist = (token) => {
    Axios.get(`${api.api}/donasi/daftar-donasi`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        // setProfile(res.data.data.profile);
        // console.log('res=>', res.data.data.donasi);
        setDonasi(res.data.data.donasi);
      })
      .catch((err) => {
        console.log('error=>', err);
      });
  };
  const capitalize = (s) => {
    if (typeof s !== 'string') return '';
    return s.charAt(0).toUpperCase() + s.slice(1);
  };
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity onPress={() => navigation.push('DonasiDetails', item)}>
        <View style={styles.donateItem}>
          <Image
            source={require('../assets/img/poverty-1028841_640.jpg')}
            style={styles.donateImg}
          />
          <View style={styles.donateItemDescCon}>
            <View style={styles.donateItemDesc}>
              <View style={styles.donateTitleCon}>
                <Text style={styles.donateItemTitle}>
                  {capitalize(item.title)}
                </Text>
              </View>
              <Text style={styles.donateItemSender}>{item.user.name}</Text>
              <Text>Dana yang dibutuhkan</Text>
              <Text style={styles.donateItemNominal}>
                {' Rp.' +
                  item.donation
                    .toFixed()
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <View style={styles.headerLogged}>
          <Ionicons
            style={styles.iconSearch}
            name="chevron-back"
            size={30}
            onPress={() => navigation.replace('Dashboard')}
          />
          <Text style={styles.headerTextLogged}>Donasi</Text>
        </View>
        {donasi == null ? (
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <ActivityIndicator size="large" color="#30475e" />
          </View>
        ) : (
          <FlatList
            data={donasi}
            renderItem={renderItem}
            keyExtractor={(item) => JSON.stringify(item.id)}
          />
        )}
      </View>
    </>
  );
};

export default donasi;

import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {Button} from '../components/Button';
import styles from '../style/style';
import colors from '../style/colors';
import api from '../api/index';
import Axios from 'axios';

import MaterialComIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const config = {
  title: 'Authentication Required',
  imageColor: colors.main,
  imageErrorColor: 'red',
  sensorDescription: 'Touch sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'cancel',
};

export default function Login({navigation}) {
  const [email, setEmail] = useState('zakkymf@gmail.com');
  const [password, setPassword] = useState('123456');

  const [encpass, setEncryptpass] = useState(true);
  const [ipassname, setIconpass] = useState('eye-off-outline');

  const [activity, setActivity] = useState(false);

  const mountedRef = useRef(true);
  const encryptText = (state, setstate, enc, setenc) => {
    setstate(state == 'eye-off-outline' ? 'eye-outline' : 'eye-off-outline');
    setenc(!enc);
  };
  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
      // const tok = await AsyncStorage.getItem('token');
      // console.log('token => ', tok);
    } catch (err) {
      console.log(err);
    }
  };
  const saveLoginMethod = async (signInMethod) => {
    try {
      await AsyncStorage.setItem('signin-method', signInMethod);
    } catch (err) {
      console.log(err);
    }
  };
  const onLoginPress = () => {
    setActivity(true);
    let data = {
      email: email,
      password: password,
    };
    Axios.post(`${api.api}/auth/login`, data, {
      timeout: 20000,
    })
      .then((res) => {
        // console.log('Login => res ', res);
        saveLoginMethod('SANBER');
        saveToken(res.data.data.token);
        Keyboard.dismiss();
        setActivity(false);
        // navigation.replace('Dashboard');
        navigation.reset({
          index: 0,
          routes: [{name: 'Dashboard'}],
        });
      })
      .catch((err) => {
        console.log('error => ', err);
        Alert.alert('OOPS', 'Please try again', [
          {
            text: 'OK',
            onPress: () => {
              Keyboard.dismiss();
              setActivity(false);
              console.log('alert close');
            },
          },
        ]);
      });
  };

  useEffect(() => {
    configureGoogleSignIn();
    mountedRef.current = false;
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '147189936259-qjob1tsi1e2t114volr5jbrbs873meft.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log('signInwithgoogle => idtoken', idToken);
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      saveLoginMethod('GOOGLE');
      // navigation.replace('Dashboard');
      navigation.reset({
        index: 0,
        routes: [{name: 'Dashboard'}],
      });
    } catch (error) {
      console.log('signin with google  => error', error);
    }
  };

  const signInWithFingerprint = () => {
    TouchID.authenticate('', config)
      .then((success) => {
        // alert('Authentication is succes');
        // navigation.replace('Dashboard');
        saveLoginMethod('FINGER');
        navigation.reset({
          index: 0,
          routes: [{name: 'Dashboard'}],
        });
      })
      .catch((error) => {
        alert('Authentication is failed');
      });
  };

  return (
    <>
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Welcome back</Text>
          <View>
            <Text style={styles.subHeadText}>let's grow your business</Text>
          </View>
        </View>
        <View style={styles.form}>
          <TextInput
            value={email}
            placeholder="Email"
            onChangeText={(email) => setEmail(email)}
            style={styles.textInput}
          />
          <View style={styles.textInputEyeContainer}>
            <TextInput
              value={password}
              secureTextEntry={encpass}
              onChangeText={(e) => setPassword(e)}
              placeholder="Password"
              style={styles.textInputEye}
            />
            <Ionicons
              style={styles.iconEye}
              name={ipassname}
              size={20}
              onPress={() =>
                encryptText(ipassname, setIconpass, encpass, setEncryptpass)
              }
            />
          </View>
          <View style={styles.buttonAlign}>
            <Button style={styles.btnRegister} onPress={() => onLoginPress()}>
              {activity == true ? (
                <ActivityIndicator size="small" color="#30475e" />
              ) : (
                <Text></Text>
              )}
              <Text style={styles.btnTextRegister}>LOGIN</Text>
            </Button>
          </View>
          <View style={styles.subFootText}>
            <Text>don't have an account yet?</Text>
            <TouchableOpacity onPress={() => navigation.replace('Register')}>
              <Text style={{marginLeft: 10, fontWeight: 'bold'}}>REGISTER</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={styles.twoBtn}>
        <Button
          style={styles.optionsBtn}
          onPress={() => signInWithFingerprint()}>
          <MaterialComIcon
            style={styles.iconEye}
            name="fingerprint"
            color="#93abd3"
            size={20}
          />
          <Text style={styles.btnTextRegister}>Fingerprint</Text>
        </Button>
        <Text>- OR -</Text>
        <Button style={styles.optionsBtn} onPress={() => signInWithGoogle()}>
          <MaterialComIcon
            style={styles.iconEye}
            name="google"
            color="#ec5858"
            size={20}
          />
          <Text style={styles.btnTextRegister}>Google</Text>
        </Button>
      </View>
    </>
  );
}

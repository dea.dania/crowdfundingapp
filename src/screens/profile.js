import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import MaterialComIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import styles from '../style/style';
import api from '../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import {GoogleSignin} from '@react-native-community/google-signin';

const {width, height} = Dimensions.get('window');
const Profile = ({navigation}) => {
  const [data, setData] = useState({});
  const [UserInfo, setUserInfo] = useState(null);
  const [isGoogle, setIsGoogle] = useState(false);
  const [pass, setPass] = useState({});
  const [isloading, setIsloading] = useState(true);
  // console.log(UserInfo);
  // console.log(isGoogle);
  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        console.log('token => ', token);
        if (token !== null) return getProfile(token);
        // console.log(Profile);
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
    // getProfile();
    getCurrentUser();
  }, []);

  const getCurrentUser = async () => {
    try {
      const usInfo = await GoogleSignin.signInSilently();
      // console.log('usinfo=>', usInfo);
      setUserInfo(usInfo);
      setIsGoogle(true);
      setPass(usInfo.user);
      setIsloading(!isloading);
      // console.log(userInfo);
      // console.log(isGoogle);
      // console.log(userInfo.name);
      // console.log(userInfo.photo);
    } catch (error) {
      setIsGoogle(false);
    }
  };

  const getProfile = (token) => {
    // Axios.get(`${api.api}/profile/get-profile`, {
    Axios.get(`https://crowdfunding.sanberdev.com/api/profile/get-profile`, {
      // timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        // console.log('res = >', res);
        setData(res.data.data.profile);
        setIsloading(!isloading);
      })
      .catch((err) => {
        console.log('error=>', {err});
      });
  };

  const onLogoutPress = async () => {
    try {
      if (UserInfo !== null) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('signin-method');

      // navigation.navigate('Login');
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    } catch {
      (err) => {
        console.log('error =>', err);
      };
    }
  };

  return (
    <>
      {/* <StatusBar backgroundColor="#00BCD4" barStyle="light-content" /> */}
      <View style={styles.container}>
        {/* Header */}
        <View style={styles.headerLogged}>
          <Ionicons
            style={styles.iconSearch}
            name="chevron-back"
            size={30}
            onPress={() => navigation.replace('Dashboard')}
          />
          <Text style={styles.headerTextLogged}>Account</Text>
        </View>
        {/* Profile : image and name */}
        <TouchableOpacity onPress={() => navigation.navigate('EditProfile')}>
          <View style={styles.profile}>
            {isloading == true ? (
              <ActivityIndicator size="small" color="#30475e" />
            ) : undefined}

            {isGoogle == false ? (
              data.photo == null ? (
                <MaterialComIcon
                  style={styles.iconEye}
                  name="face-profile"
                  size={30}
                />
              ) : (
                <Image
                  key={new Date().getTime()}
                  source={{
                    uri: api.home + data.photo + '?' + new Date(),
                    cache: 'reload',
                    headers: {Pragma: 'no-cache'},
                  }}
                  style={styles.img}
                />
              )
            ) : (
              <Image
                key={new Date().getTime()}
                source={{uri: UserInfo && UserInfo.user && UserInfo.user.photo}}
                style={styles.img}
              />
            )}
            <Text style={styles.profileText}>
              {isGoogle == false
                ? data.name
                : UserInfo && UserInfo.user && UserInfo.user.name}
            </Text>
          </View>
        </TouchableOpacity>
        {/* Saldo */}
        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.subItemSaldo}>
              {/* <Wallet width={50} height={50} /> */}
              <Ionicons
                style={styles.iconEye}
                name="wallet-outline"
                size={20}
              />
              <View style={{marginLeft: -(width * 0.3)}}>
                <Text style={styles.itemText}>Saldo</Text>
              </View>
              <Text style={styles.itemText}>Rp. 999.999.999</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.subItem}>
              {/* <Settings width={50} height={50} /> */}
              <Ionicons
                style={styles.iconEye}
                name="settings-outline"
                size={20}
              />
              <Text style={styles.itemText}>Pengaturan</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Help')}>
            <View style={styles.subItem}>
              {/* <Help width={50} height={50} /> */}
              <Ionicons style={styles.iconEye} name="help-outline" size={20} />
              <Text style={styles.itemText}>Bantuan</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.subItem}>
              {/* <TermsCond width={50} height={50} /> */}
              <Ionicons
                style={styles.iconEye}
                name="md-document-text-outline"
                size={20}
              />
              <Text style={styles.itemText}>Syarat dan Ketentuan</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.item}>
          <TouchableOpacity onPress={() => onLogoutPress()}>
            <View style={styles.subItem}>
              {/* <Logout width={50} height={50} /> */}
              <MaterialComIcon style={styles.iconEye} name="logout" size={20} />
              <Text style={styles.itemText}>Keluar</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};
export default Profile;

import React, {useState, useEffect} from 'react';
import {
  View,
  Keyboard,
  Alert,
  TextInput,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import styles from '../style/style';
import {Button} from '../components/Button';
import api from '../api/index';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';

const register = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [fullname, setFullname] = useState('');
  const [disable, setDisable] = useState(true);
  const [activity, setActivity] = useState(false);
  const onRegisterPress = () => {
    setActivity(true);
    if (email.trim() == '' || fullname.trim() == '') {
      Alert.alert('OOPS', 'Data belum lengkap! ', [
        {
          text: 'MENGERTI',
          onPress: () => {
            setActivity(false);
            console.log('alert close');
          },
        },
      ]);
    } else {
      let data = {
        email: email,
        name: fullname,
      };
      Axios.post(`${api.api}/auth/register`, data, {
        timeout: 20000,
      })
        .then((res) => {
          console.log('Login => res ', res);
          Keyboard.dismiss();
          setActivity(false);
          navigation.push('OTP', data);
        })
        .catch((err) => {
          console.log('error => ', err);
          Alert.alert(
            'OOPS',
            'Email tidak dikenal atau email sudah terdaftar! ',
            [
              {
                text: 'OK',
                onPress: () => {
                  Keyboard.dismiss();
                  setActivity(false);
                  console.log('alert close');
                },
              },
            ],
          );
        });
    }
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>DAFTAR</Text>
      </View>
      <View style={styles.form}>
        <TextInput
          value={email}
          placeholder="Email"
          onChangeText={(email) => setEmail(email)}
          style={styles.textInput}
        />
        <TextInput
          value={fullname}
          onChangeText={(fullname) => setFullname(fullname)}
          placeholder="Nama Lengkap"
          style={styles.textInput}
        />
        <TouchableOpacity disabled={disable}>
          <Button style={styles.btnRegister} onPress={() => onRegisterPress()}>
            {activity == true ? (
              <ActivityIndicator size="small" color="#edf285" />
            ) : (
              <Text></Text>
            )}
            <Text style={styles.btnTextRegister}>DAFTAR</Text>
          </Button>
        </TouchableOpacity>
        <View style={styles.subFootText}>
          <Text>sudah memiliki akun?</Text>
          <TouchableOpacity onPress={() => navigation.replace('Login')}>
            <Text style={{marginLeft: 10, fontWeight: 'bold'}}>LOGIN</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default register;

import React, {useState, useEffect} from 'react';
import {
  View,
  Keyboard,
  Alert,
  TextInput,
  Text,
  TouchableOpacity,
} from 'react-native';
import styles from '../style/style';
import {Button} from '../components/Button';
import api from '../api/index';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
const SetupPassword = ({navigation, route}) => {
  const [email, setEmail] = useState(route.params.email);
  const [fullname, setFullname] = useState(route.params.name);

  const [encpass, setEncryptpass] = useState(true);
  const [encpasscom, setEncryptpasscom] = useState(true);
  console.log(fullname);
  const [ipassname, setIconpass] = useState('eye-off-outline');
  const [ipasscomname, setIconpasscom] = useState('eye-off-outline');

  const [activity, setActivity] = useState(false);

  const [pass, setPass] = useState('');
  const [passcom, setPasscom] = useState('');

  const encryptText = (state, setstate, enc, setenc) => {
    setstate(state == 'eye-off-outline' ? 'eye-outline' : 'eye-off-outline');
    setenc(!enc);
  };

  const onSavePress = () => {
    console.log(pass);
    console.log(passcom);
    if (pass.trim() == '' || passcom.trim() == '') {
      Alert.alert('OOPS', 'Data belum lengkap! ', [
        {
          text: 'MENGERTI',
          onPress: () => {
            setActivity(false);
            console.log('alert close');
          },
        },
      ]);
    } else if (pass === passcom) {
      let data = {
        email: email,
        password: pass,
        password_confirmation: passcom,
      };
      Axios.post(`${api.api}/auth/update-password`, data, {
        timeout: 20000,
      })
        .then((res) => {
          console.log('Login => res ', res);
          Keyboard.dismiss();
          Alert.alert('BERHASIL', 'Silahkan login dengan akun terdaftar! ', [
            {
              text: 'OK',
              onPress: () => {
                navigation.replace('Login');
              },
            },
          ]);
        })
        .catch((err) => {
          console.log('error => ', err);
        });
    } else {
      Alert.alert('Gagal', 'Pastikan password dan password konfirmasi sama! ', [
        {
          text: 'OK',
          onPress: () => {
            console.log('password tidak sama');
          },
        },
      ]);
    }
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>SETUP PASSWORD</Text>
        <View>
          <Text style={styles.subHeadText}>
            Atur password untuk email {email}
          </Text>
        </View>
      </View>
      <View style={styles.form}>
        <View style={styles.textInputEyeContainer}>
          <TextInput
            value={pass}
            secureTextEntry={encpass}
            onChangeText={(e) => setPass(e)}
            placeholder="Password"
            style={styles.textInputEye}
          />
          <Ionicons
            style={styles.iconEye}
            name={ipassname}
            size={20}
            onPress={() =>
              encryptText(ipassname, setIconpass, encpass, setEncryptpass)
            }
          />
        </View>
        <View style={styles.textInputEyeContainer}>
          <TextInput
            value={passcom}
            secureTextEntry={encpasscom}
            onChangeText={(e) => setPasscom(e)}
            placeholder="Password Konfirmasi"
            style={styles.textInputEye}
          />
          <Ionicons
            style={styles.iconEye}
            name={ipasscomname}
            size={20}
            onPress={() =>
              encryptText(
                ipasscomname,
                setIconpasscom,
                encpasscom,
                setEncryptpasscom,
              )
            }
          />
        </View>
        <Button style={styles.btnRegister} onPress={() => onSavePress()}>
          <Text style={styles.btnTextRegister}>SIMPAN</Text>
        </Button>
      </View>
    </View>
  );
};

export default SetupPassword;

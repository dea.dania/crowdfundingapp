import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Alert,
  Modal,
  StatusBar,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {Button} from '../components/Button';

import {RNCamera} from 'react-native-camera';
import {TextInputMask} from 'react-native-masked-text';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from '../style/style';
import colors from '../style/colors';
import api from '../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
const newDonasi = ({navigation}) => {
  let input = useRef(null);
  let camera = useRef(null);
  const [token, setToken] = useState('');
  const [isVisible, setIsVisible] = useState(false);
  const [photo, setPhoto] = useState(null);
  const [title, setTitle] = useState('');
  const [desc, setDesc] = useState('');
  const [nominal, setNom] = useState(1000000);

  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token !== null) return setToken(token);
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
  }, []);
  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.current.takePictureAsync(options);
      setPhoto(data);
      setIsVisible(false);
      console.log('editaccount=< data > ', data);
    }
  };
  const onSavePress = async () => {
    if (photo == null || title == null || desc == null || nominal === 0) {
      alert('data belum lengkap');
    }
    const formData = new FormData();
    formData.append('title', title);
    formData.append('description', desc);
    formData.append('donation', nominal);
    formData.append('photo', {
      uri: photo.uri,
      name: 'photo.jpg',
      type: 'image/jpg',
    });
    console.log(formData);
    Axios.post(`${api.api}/donasi/tambah-donasi`, formData, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'aplication/json',
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((res) => {
        console.log('res = >', res);
        alert('upload donasi baru berhasil');
        // console.log(res.request);
        // await AsyncStorage.removeItem('token');
        // await AsyncStorage.setItem('token', res.data.data.profile)
        // navigation.navigate('Profile');
        // navigation.replace('Profile');
      })
      .catch((err) => {
        console.log('error=>', {err});
        alert('upload donasi baru gagal');
      });
  };
  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera style={{flex: 1}} type="back" ref={camera}></RNCamera>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              backgroundColor: colors.black,
            }}>
            <TouchableOpacity onPress={() => takePicture()}>
              <View style={styles.btnToggleCamera}>
                <Ionicons
                  name="camera-outline"
                  size={50}
                  color={colors.white}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  return (
    <>
      <View style={styles.container}>
        <View style={styles.headerLogged}>
          <Ionicons
            style={styles.iconSearch}
            name="chevron-back"
            size={30}
            onPress={() => navigation.goBack()}
          />
          <Text style={styles.headerTextLogged}>Buat Donasi</Text>
        </View>
        {photo == null ? (
          <View style={styles.imgNewDonate}>
            <Ionicons
              name="camera-outline"
              size={50}
              color={colors.white}
              onPress={() => setIsVisible(true)}
            />
          </View>
        ) : (
          <Image
            source={{
              uri: photo.uri,
              cache: 'reload',
              headers: {Pragma: 'no-cache'},
            }}
            style={styles.imgNewDonate}
          />
        )}
        <View style={styles.form}>
          <Text style={styles.textLabel}>Judul</Text>
          <TextInput
            value={title}
            placeholder="Judul"
            onChangeText={(val) => setTitle(val)}
            style={styles.textInputDonasi}
          />
          <Text style={styles.textLabel}>Deskripsi</Text>
          <TextInput
            value={desc}
            placeholder="Deskripsi"
            multiline={true}
            numberOfLines={7}
            onChangeText={(text) => setDesc({text})}
            style={styles.textAreaInput}
          />
          <Text style={styles.textLabel}>Dana yang dibutuhkan</Text>

          <TextInputMask
            type={'cpf'}
            // dont forget to set the "value" and "onChangeText" props
            value={nominal}
            onChangeText={(val) => setNom(nominal)}
          />
          <Button style={styles.btnRegister} onPress={() => onSavePress()}>
            <Text style={styles.btnTextRegister}>BUAT</Text>
          </Button>
        </View>
      </View>

      {renderCamera()}
    </>
  );
};

export default newDonasi;

import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Animated,
  Image,
  Dimensions,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import colors from '../style/colors';

const {width, height} = Dimensions.get('window');

const Splashscreens = () => {
  const fadeOut = useRef(new Animated.Value(1)).current;
  const fadeIn = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeOut, {
      toValue: 0,
      duration: 3000,
      useNativeDriver: false,
    }).start();
    Animated.timing(fadeIn, {
      toValue: 1,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  }, [fadeOut, fadeIn]);

  const transformY = fadeIn.interpolate({
    inputRange: [0, 1],
    outputRange: [height, height / 2],
  });

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.black} barStyle="dark-content" />
        <Animated.View
          style={[
            styles.logo,
            {opacity: fadeIn, transform: [{translateY: transformY}]},
          ]}>
          <Text style={styles.textLogo}>CrowdFunding</Text>
        </Animated.View>
        <Animated.View style={[styles.quotesContainer, {opacity: fadeOut}]}>
          <Image
            source={require('../assets/img/undraw_super_thank_you_obwk.png')}
            style={{width: height * 0.3, height: height * 0.3}}
            resizeMethod="auto"
            resizeMode="contain"
          />
          <ActivityIndicator size="large" color="#fffa65" />
          <Text style={styles.quotes}>
            "Berbagi adalah kebahagiaan yang sesungguhnya"
          </Text>
        </Animated.View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  quotesContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  quotes: {
    fontSize: 14,
    color: colors.black,
  },
  logo: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  textLogo: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.black,
  },
});

export default Splashscreens;

import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  ScrollView,
  FlatList,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Button} from '../components/Button';
import {SliderBox} from 'react-native-image-slider-box';

import MaterialComIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleIcon from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from '../style/style';
import api from '../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const data = [
  {
    id: 1,
    image: require('../assets/img/poverty-1028841_640.jpg'),
  },
  {
    id: 2,
    image: require('../assets/img/buddhists-453317_640.jpg'),
  },
  {
    id: 3,
    image: require('../assets/img/blood-5053771_640.jpg'),
  },
];

const home = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(true);

  const [keyword, setKeyword] = useState('');
  const [text, setText] = useState(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim  ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut  aliquip ex ea commodo consequat. Duis aute irure dolor in  reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla  pariatur. Excepteur sint occaecat cupidatat non proident, sunt in  culpa qui officia deserunt mollit anim id est laborum.',
  );
  const [img, setImages] = useState([
    require('../assets/img/blood-5053771_640.jpg'),
    require('../assets/img/buddhists-453317_640.jpg'),
    require('../assets/img/poverty-1028841_640.jpg'),
  ]);
  useEffect(() => {
    const getToken = async () => {
      try {
        const signInMethod = await AsyncStorage.getItem('signin-method');
        if (signInMethod == null)
          return navigation.reset({
            index: 0,
            routes: [{name: 'Login'}],
          });
        setIsLoading(false);
      } catch (err) {
        console.log('err => ', err);
      }
    };
    getToken();
  }, []);
  if (isLoading) return null;
  const renderUrgentItem = ({item}) => {
    return (
      <>
        <View style={styles.urgentDonateItem}>
          <Image
            source={item.image}
            style={styles.donateUrgImg}
            resizeMethod="auto"
            resizeMode="contain"
          />
          <View style={styles.textUrgCon}>
            <Text style={styles.urgentDonateText}>{text.substring(0, 50)}</Text>
          </View>
        </View>
      </>
    );
  };
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.searchContainer}>
          <TextInput
            value={keyword}
            placeholder="Search"
            onChangeText={(key) => setKeyword(key)}
            style={styles.textInputEye}
          />
          <Ionicons style={styles.iconSearch} name="search" size={20} />
        </View>
        <View style={styles.payContainer}>
          <Button style={styles.payItemBtnsSaldo}>
            <View style={styles.paySaldoItem}>
              <Ionicons
                style={styles.iconSearch}
                name="wallet-outline"
                size={20}
              />
              <Text>Saldo</Text>
            </View>
            <Text>Rp.0,-</Text>
          </Button>
          <Button style={styles.payItemBtns}>
            <MaterialComIcon
              style={styles.iconSearch}
              name="plus-box-outline"
              size={20}
            />
            <Text style={styles.payItemText}>isi</Text>
          </Button>
          <Button style={styles.payItemBtns}>
            <MaterialComIcon
              style={styles.iconSearch}
              name="history"
              size={20}
            />
            <Text style={styles.payItemText}>Riwayat</Text>
          </Button>
          <Button style={styles.payItemBtns}>
            <MaterialComIcon
              style={styles.iconSearch}
              name="vector-square"
              size={20}
            />
            <Text style={styles.payItemText}>Lainnya</Text>
          </Button>
        </View>
        <SliderBox
          autoplay
          circleLoop
          images={img}
          sliderBoxHeight={200}
          dotColor="#FFEE58"
          inactiveDotColor="#90A4AE"
          ImageComponentStyle={{width: '92%', borderRadius: 5}}
        />
        <View style={styles.menuContainer}>
          <Button
            style={styles.menuItemBtns}
            onPress={() => navigation.navigate('Donasi')}>
            <MaterialComIcon
              style={styles.iconSearch}
              name="cash-usd-outline"
              size={30}
            />
            <Text style={styles.menuItemText}>DONASI</Text>
          </Button>
          <Button
            style={styles.menuItemBtns}
            onPress={() => navigation.navigate('Statistic')}>
            <SimpleIcon style={styles.iconSearch} name="graph" size={30} />
            <Text style={styles.menuItemText}>STATISTIK</Text>
          </Button>
          <Button
            style={styles.menuItemBtns}
            onPress={() => navigation.navigate('HistoryDonasi')}>
            <MaterialComIcon
              style={styles.iconSearch}
              name="history"
              size={30}
            />
            <Text style={styles.menuItemText}>RIWAYAT</Text>
          </Button>
          <Button
            style={styles.menuItemBtns}
            onPress={() => navigation.navigate('NewDonasi')}>
            <Ionicons
              style={styles.iconSearch}
              name="ios-heart-outline"
              size={30}
            />
            <Text style={styles.menuItemText}>BANTU</Text>
          </Button>
        </View>
        <View style={styles.urgentDonateContainer}>
          <FlatList
            horizontal={true}
            data={data}
            renderItem={renderUrgentItem}
            keyExtractor={(item) => JSON.stringify(item.id)}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default home;

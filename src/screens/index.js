import Intro from './intro';
import Splash from './splash';
import Register from './register';
import OTP from './otp';
import Login from './login';
import SetupPassword from './setupPassword';
import Profile from './profile';
import Home from './home';
import Donasi from './donasi';
import DonasiDetails from './donasiDetails';
import Payment from './payment';
import Help from './help';
import Statistic from './statistic';
import Inbox from './inbox';
import HistoryDonasi from './historyDonasi';
import NewDonasi from './donasiBaru';
import EditProfile from './editProfile'
export {
  Intro,
  Register,
  Splash,
  OTP,
  Login,
  SetupPassword,
  Profile,
  EditProfile,
  Home,
  Donasi,
  DonasiDetails,
  NewDonasi,
  Payment,
  Help,
  Statistic,
  Inbox,
  HistoryDonasi,
};

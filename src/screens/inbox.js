import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import database from '@react-native-firebase/database';
import {GiftedChat} from 'react-native-gifted-chat';

import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from '../style/style';
import api from '../api';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';

const inbox = ({navigation}) => {
  const [data, setData] = useState([]);
  const [messages, setMessages] = useState([]);
  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        // console.log('token => ', token);
        if (token !== null) return getProfile(token);
        // console.log(Profile);
      } catch (err) {
        console.log('err => ', err);
      }
    };
    getToken();
    getProfile();
    onRef();
    // jika komponen ilang, maka menghentikan request
    return () => {
      const db = database().ref('messages');
      if (db) {
        db.off();
      }
    };
  }, []);

  const getProfile = (token) => {
    Axios.get(`${api.api}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
        Accept: 'aplication/json',
        'Content-Type': 'aplication/json',
      },
    })
      .then((res) => {
        console.log('res = >', res);
        setData(res.data.data.profile);
      })
      .catch((err) => {
        console.log('error=>', err);
      });
  };
  const onRef = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, value),
        );
      });
  };
  const onSendPress = (messages = []) => {
    for (let i = 0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user,
      });
    }
  };
  return (
    <View style={styles.container}>
      <View style={styles.headerLogged}>
        <Ionicons
          style={styles.iconSearch}
          name="chevron-back"
          size={30}
          onPress={() => navigation.replace('Dashboard')}
        />
        <Text style={styles.headerTextLogged}>Inbox</Text>
      </View>
      <GiftedChat
        messages={messages}
        onSend={(messages) => onSendPress(messages)}
        user={{
          _id: data.id,
          name: data.name,
          avatar: `${api.home}${data.photo}`,
        }}
        showUserAvatar
      />
    </View>
  );
};

export default inbox;

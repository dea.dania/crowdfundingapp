import React, {useEffect} from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';
import {Button} from '../components/Button';
import PaperOnboarding, {
  PaperOnboardingItemType,
} from '@gorhom/paper-onboarding';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import ThankYou from '../assets/img/undraw_super_thank_you_obwk.svg';
import GoodTeam from '../assets/img/undraw_good_team_m7uu.svg';
import Deliver from '../assets/img/undraw_deliveries_131a.svg';
import Respect from '../assets/img/respect.svg';

const {width, height} = Dimensions.get('window');

const data: PaperOnboardingItemType[] = [
  {
    title: 'Gotong royong',
    description: 'Bersama-sama sedikit demi sedikit',
    backgroundColor: '#698FB8',
    image: <GoodTeam width={width * 0.5} height={width * 0.5} />,
  },
  {
    title: 'Amanah',
    description: 'Menyampaikan dengan amanah',
    backgroundColor: '#6CB2B8',
    image: <Deliver width={width * 0.5} height={width * 0.5} />,
  },
  {
    title: 'Berbagi',
    description: 'Berbagi kebahagiaan',
    backgroundColor: '#FF9D65',
    image: <ThankYou width={width * 0.5} height={width * 0.5} />,
  },
];
export default function intro({navigation}) {
  const handleOnClosePress = () => {
    console.log('navigate to other screen');
    navigation.replace('Register');
  };
  useEffect(() => {
    async function getShow() {
      try {
        await AsyncStorage.setItem('first', 'false');
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getShow();
  }, []);
  return (
    <>
      <PaperOnboarding
        data={data}
        onCloseButtonPress={handleOnClosePress}
        closeButtonText="REGISTER.."
        closeButtonTextStyle={styles.closeBtn}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'yellow',
  },
  closeBtn: {
    fontSize: 20,
  },
});

import React from 'react';
import {View, Text} from 'react-native';
import {WebView} from 'react-native-webview';
import styles from '../style/style';
const payment = ({route}) => {
  console.log('route=>', route);
  return (
    <WebView
      style={styles.container}
      source={{uri: route.params.midtrans.redirect_url}}
    />
  );
};

export default payment;

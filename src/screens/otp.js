import React, {useState} from 'react';
import {View, Text, TouchableOpacity, Alert, Keyboard} from 'react-native';
import InputCode from 'react-native-input-code';
import styles from '../style/style';
import colors from '../style/colors';
import {Button} from '../components/Button';
import api from '../api/index';
import Axios from 'axios';
import OTPInputView from '@twotalltotems/react-native-otp-input';
const otp = ({route, navigation}) => {
  const [email, setEmail] = useState(route.params.email);
  const [fullname, setFullname] = useState(route.params.fullname);

  const [code, setCode] = useState('');
  const regenerateCode = () => {
    let passdata = {
      email: email,
    };
    Axios.post(`${api.api}/auth/regenerate-otp`, passdata, {
      timeout: 20000,
    }).then((res) => {
      console.log('Verifikasi=> res ', res);
      Alert.alert('OOPS', 'Silahkan periksa kotak masuk email Anda', [
        {
          text: 'OK',
          onPress: () => {
            Keyboard.dismiss();
            console.log('alert close');
          },
        },
      ]);
    });
  };
  const onRegisterPress = () => {
    // setCode(Number(code));
    // console.log(Number(code));
    console.log(typeof code);
    let data = {
      otp: code,
      email: email,
      name: fullname,
    };
    Axios.post(
      `${api.api}/auth/verification`,
      {otp: data.otp},
      {
        timeout: 20000,
      },
    )
      .then((res) => {
        console.log('Verifikasi=> res ', res);

        Keyboard.dismiss();
        if (res.data.response_message !== 'berhasil diverifikasi') {
          const msg = res.data.response_message;
          Alert.alert('OOPS', msg, [
            {
              text: 'OK',
              onPress: () => {
                Keyboard.dismiss();
                console.log('alert close');
              },
            },
          ]);
        } else if (res.data.response_message == 'berhasil diverifikasi') {
          navigation.push('SetupPassword', data);
        }
      })
      .catch((err) => {
        console.log('error => ', err);
      });
  };

  return (
    <>
      <View style={styles.header}>
        <Text style={styles.headerText}>VERIFIKASI</Text>
        <Text style={styles.subHeadText}>{email}</Text>
      </View>
      <View style={styles.form}>
        <OTPInputView
          onCodeChanged={(code) => {
            setCode(code);
          }}
          style={styles.otpStyle}
          pinCount={6}
          autoFocusOnLoad
          onCodeFilled={(code) => {
            console.log(`Code is ${code}, you are good to go!`);
          }}
        />
        <Button style={styles.btnRegister} onPress={() => onRegisterPress()}>
          <Text style={styles.btnTextRegister}>VERIFIKASI</Text>
        </Button>
        <View style={styles.subFootText}>
          <TouchableOpacity onPress={() => regenerateCode()}>
            <Text
              style={{marginLeft: 10, fontWeight: 'bold', color: '#6CB2B8'}}>
              Generate ulang kode
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

export default otp;

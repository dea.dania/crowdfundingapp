import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import MaterialComIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import MapboxGL from '@react-native-mapbox-gl/maps';

import styles from '../style/style';
import {FlatList} from 'react-native-gesture-handler';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoiZGVhZGFuaWEiLCJhIjoiY2tobHNmbjZoMWJpOTJwcWt3cWphczJlZiJ9.1KIAnGpmMI-MMSyXXIdnYQ',
);
const coordinates = [
  [107.58011, -6.890066],
  [106.819449, -6.218465],
  [110.365231, -7.795766],
];

const dataHelp = [
  {iconname: 'location-outline', desc: 'Jakarta, Bandung, Yogyakarta'},
  {iconname: 'mail-outline', desc: 'customer_service@crowdfunding.com'},
  {iconname: 'call-outline', desc: '(021) 777 - 888'},
];
const help = ({navigation}) => {
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (error) {
        console.log('error=>', error);
      }
    };
    getLocation();
  });

  const renderListHelp = ({item}) => {
    return (
      <TouchableOpacity>
        <View style={styles.subItem}>
          <Ionicons style={styles.iconEye} name={item.iconname} size={20} />
          <Text style={styles.itemText}>{item.desc}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <View style={styles.headerLogged}>
        <Ionicons
          style={styles.iconSearch}
          name="chevron-back"
          size={30}
          onPress={() => navigation.replace('Dashboard')}
        />
        <Text style={styles.headerTextLogged}>Bantuan</Text>
      </View>
      <MapboxGL.MapView style={{flex: 1, marginTop: -16}}>
        {/* menunjukkan lokasi user */}
        {/* <MapboxGL.UserLocation visible={true} /> */}
        {/* <MapboxGL.Camera followUserLocation={true} /> */}

        <MapboxGL.Camera
          centerCoordinate={coordinates[0]}
          zoomLevel={5}
          // bounds={bounds}
        />
        {/* memberikan pin location */}
        {coordinates.map((coor, index) => (
          <MapboxGL.PointAnnotation
            id={'pointSatu' + index}
            coordinate={coor}
            key={index}>
            <MapboxGL.Callout
              title={`Longtitude ${coordinates[0][0]} \n Latitude ${coordinates[0][1]}`}
            />
          </MapboxGL.PointAnnotation>
        ))}
      </MapboxGL.MapView>
      <View style={styles.container}>
        <View style={styles.item}>
          <FlatList
            data={dataHelp}
            renderItem={(item) => renderListHelp(item)}
          />
        </View>
      </View>
    </View>
  );
};

export default help;

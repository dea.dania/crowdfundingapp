import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, processColor} from 'react-native';
import MaterialComIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {BarChart} from 'react-native-charts-wrapper';
import styles from '../style/style';
import colors from '../style/colors';
const ydata = [
  {y: 100},
  {y: 60},
  {y: 90},
  {y: 45},
  {y: 67},
  {y: 32},
  {y: 150},
  {y: 70},
  {y: 40},
  {y: 89},
];
const statistic = (navigation) => {
  const [legend, setLegend] = useState({
    enabled: false,
    textSize: 14,
    form: 'SQUARE',
    formSize: 14,
    xEntrySpace: 10,
    yEntrySpace: 5,
    formToTextSpace: 5,
    wordWrapEnable: true,
    maxSizePercent: 0.5,
  });
  const [chart, setChart] = useState({
    data: {
      dataSets: [
        {
          values: ydata,
          label: '',
          config: {
            colors: [processColor(colors.main)],
            stackLabels: [],
            drawFilled: false,
            drawVales: false,
          },
        },
      ],
    },
  });
  const [xAxis, setXAxis] = useState({
    valueFormatter: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'Mei',
      'Jun',
      'Jul',
      'Agu',
      'Sep',
      'Okt',
      //   'Nov',
      //   'Des',
    ],
    position: 'BOTTOM',
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: new Date().getMonth() + 0.5,
    spaceBetweenLabels: 0,
    labelRotationAngele: -45.0,
    limitLines: [{limit: 115, lineColor: processColor('red'), lineWidth: 1}],
  });
  const [yAxis, setYAxis] = useState({
    left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false,
    },
    right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false,
    },
  });

  return (
    <View style={styles.container}>
      <View style={styles.headerLogged}>
        <Ionicons
          style={styles.iconSearch}
          name="chevron-back"
          size={30}
          onPress={() => navigation.replace('Dashboard')}
        />
        <Text style={styles.headerTextLogged}>Statistik</Text>
      </View>
      <View style={styles.container}>
        <BarChart
          data={chart.data}
          style={styles.chartContainer}
          yAxis={yAxis}
          xAxis={xAxis}
          chartDescription={{text: ''}}
          doubleTapToZoomEnabled={false}
          legend={legend}
          pinchZoom={false}
          marker={{
            enabled: true,
            markerColor: colors.lightGrey,
            textColor: 'yellow',
            textSize: 14,
          }}
        />
      </View>
    </View>
  );
};

export default statistic;

import React, {useState, useEffect} from 'react';
import {View, Text, ActivityIndicator, FlatList} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from '../style/style';
import api from '../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const historyDonasi = ({navigation}) => {
  const [riwayat, setRiwayat] = useState(null);
  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token !== null) return getTransactionList(token);
      } catch (err) {
        console.log('err => ', err);
      }
    }
    getToken();
  }, []);
  const getTransactionList = (token) => {
    Axios.get(`${api.api}/donasi/riwayat-transaksi`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        console.log('res=>', res);
        setRiwayat(res.data.data.riwayat_transaksi);
      })
      .catch((err) => {
        console.log('error=>', err);
      });
  };
  const renderItem = ({item}) => {
    return (
      <View style={styles.historyctionItem}>
        <Text style={styles.historyctionItemTitle}>
          Total
          {' Rp.' +
            item.amount.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
        </Text>
        <Text>Donation ID : {item.order_id}</Text>
        <Text>Tanggal Transaksi :{item.created_at}</Text>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <View style={styles.headerLogged}>
        <Ionicons
          style={styles.iconSearch}
          name="chevron-back"
          size={30}
          onPress={() => navigation.goBack()}
        />
        <Text style={styles.headerTextLogged}>Riwayat Donasi</Text>
      </View>
      {riwayat == null ? (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-around',
            alignItems: 'center',
          }}>
          <ActivityIndicator size="large" color="#30475e" />
        </View>
      ) : (
        <FlatList
          data={riwayat}
          renderItem={renderItem}
          keyExtractor={(item) => JSON.stringify(item.id)}
        />
      )}
    </View>
  );
};

export default historyDonasi;

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import firebase from '@react-native-firebase/app';
import {LogBox} from 'react-native';
import codePush from 'react-native-code-push';

LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
LogBox.ignoreAllLogs(); //Ignore all log notificationsconsole.log('onReceived => notification',notification);
var firebaseConfig = {
  apiKey: 'AIzaSyBjWKc9QGp44Eb6pn84a6UogDiKjGdUVII',
  authDomain: 'crowdfundingapp-1eaab.firebaseapp.com',
  databaseURL: 'https://crowdfundingapp-1eaab.firebaseio.com',
  projectId: 'crowdfundingapp-1eaab',
  storageBucket: 'crowdfundingapp-1eaab.appspot.com',
  messagingSenderId: '147189936259',
  appId: '1:147189936259:web:c5ed6e26ee3a2f21ddebb6',
  measurementId: 'G-7YX9DQ7BSD',
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
// firebase.initializeApp(firebaseConfig);
// firebase.analytics();
import OneSignal from 'react-native-onesignal';
import Appsrc from './src/index';
// import Appsrc from './src/intro';
const App: () => React$Node = () => {
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init('b1c065a2-f755-40fd-a0e7-29f5caa5d444', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2);
    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);

    codePush.sync(
      {
        updateDialog: true,
        installMode: codePush.InstallMode.IMMEDIATE,
      },
      SyncStatus,
    );

    // componentWilUnmount
    return () => {
      OneSignal.removeEventListener('received', onReceived);
      OneSignal.removeEventListener('opened', onOpened);
      OneSignal.removeEventListener('ids', onIds);
    };
  }, []);

  const SyncStatus = (status) => {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('checking for update');
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('downloading package');
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        console.log('up to date');
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('installing update');
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert('Notification', 'Update installed');
        break;
      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log('awaiting user action');
        break;
      default:
        break;
    }
  };

  const onReceived = (notification) => {
    console.log('onReceived => notification', notification);
  };
  const onOpened = (openResult) => {
    console.log('onOpened => openResult', openResult);
  };
  const onIds = (device) => {
    console.log('onIds => device', device);
  };
  return (
    <>
      <Appsrc />
    </>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
